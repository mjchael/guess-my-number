'use strict';

localStorage.getItem("highscore") === null? localStorage.setItem("highscore", 0):null;


let score = Number(document.querySelector('.score').textContent);
const highScore = () => localStorage.getItem("highscore");

const updateMessage = (message) => document.querySelector('.message').textContent = message;
const updateGeneratedNumber = () => Math.trunc(Math.random() * 20 );
const getGuess = () => Number(document.querySelector('.guess').value);
const updateHighScoreIfAny = (score) => {
    if (score > highScore()) {
        localStorage.setItem("highscore", score)
    }
    document.querySelector('.highscore').textContent = highScore();
};
document.querySelector('.highscore').textContent = highScore();


const updateScore = (currentScore) => {
    document.querySelector('.score').textContent = currentScore;
    score = currentScore;
};

const showNumber = () => document.querySelector('.number').textContent = generatedNumber.toString();

const hideNumber = () => document.querySelector('.number').textContent = "?";

let generatedNumber = updateGeneratedNumber();


document.querySelector('.check').addEventListener(
    'click', () => {
        const guess = getGuess();
        if (guess>= 0) {
            if (guess === generatedNumber) {
                updateHighScoreIfAny(score);
                updateMessage('You win!');
                document.querySelector('body').style.backgroundColor = '#60b347';
                document.querySelector('.number').style.width = '30rem';
                showNumber();
                document.querySelector('.check').disabled = true;
            } else {
                if (guess > generatedNumber) {
                    updateMessage('Guess too high!');
                } else {
                    updateMessage('Guess too low!');
                }

                score--;
                updateScore(score);
                if (score <= 0) {
                    updateMessage('You Lose!');
                    document.querySelector('.check').disabled = true;
                }
            }
        } else {
            updateMessage('Choose a number between 0 and 20');
        }
    }
);


document.querySelector('.again').addEventListener(
    'click', () => {
        generatedNumber = Math.trunc(Math.random() * 20);
        updateMessage('Guess!');
        updateScore(20);
        // document.querySelector('.check').setAttribute('enabled', "true");
        hideNumber();
        document.querySelector('.check').disabled = false;
        document.querySelector('.guess').value = '';

        document.querySelector('body').style.backgroundColor = '#222';
        document.querySelector('.number').style.width = '15rem';
    }
);
